# trojancleanup

Cleans up a certain trojan (or any trojan really) that injects VBScript at the end of HTML files.
 
## Usage

Install dependencies first if they don't exist.
```
cd path/to/trojancleanup
npm install
```

```
cd infected_directory
node path/to/trojancleanup/start.js
```
